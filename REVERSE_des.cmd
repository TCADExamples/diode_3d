

File {
    Grid       = "n@node|DIODE3D@_msh.tdr"
    Current    = "@plot@"
    Plot       = "@tdrdat@"
    output = "@log@"
    parameter = "@parameter@"
}



Electrode {
	!(
	for {set i 0} {$i<@NY@} {incr i} {
			for {set j 0} {$j<@NX@} {incr j} {
					puts "\{name = \"cathode${j}${i}\"   voltage = 0.0   eRecVelocity=1e7 hRecVelocity=1e7 \}"	
			}
	}
	)!
	
	{name = "anode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }	
		
}


Physics { 
	##AreaFactor=1e8
	##Fermi
 }
        
 Physics (material="Silicon") { 

 	Mobility( 
		DopingDep(Unibo)
            	HighFieldsat(GradQuasiFermi)
 	  	)
  	Recombination( 
		 SRH(DopingDep TempDep)
		 Auger
  		##hAvalanche(UniBo) eAvalanche(UniBo)  		
  		hAvalanche eAvalanche 		
		)
  	EffectiveIntrinsicDensity(OldSlotboom)

  	 	Traps(
  			(
  			name="state1" acceptor conc=@<fluence*1.613>@
  			Level FromConductionBand	EnergyMid=0.42
  			eXsection=2E-15  hXsection=2E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state2" acceptor conc=@<fluence*100.0>@
  			Level FromConductionBand	EnergyMid=0.46
  			eXsection=5E-15  hXsection=5E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state3" donor conc=@<fluence*0.9>@
  			Level FromValenceBand	EnergyMid=0.36
  			eXsection=2.5E-14  hXsection=2.5E-15
			##eJfactor=1.0 hJfactor=1.0
			)
		)



} 




CurrentPlot {
eLifeTime(Maximum(material="Silicon"))
hLifeTime(Maximum(material="Silicon"))
eAvalanche(Maximum(material="Silicon"))
hAvalanche(Maximum(material="Silicon"))
}

Math {

	##CDensityMin=1e-100
	Extrapolate
    Derivatives
  Avalderivatives
  Digits=7
  Notdamped=1000
  Iterations=15
  RelerrControl
  ErrRef(electron)=1e6
  ErrRef(hole)=1e6
  RhsMin=1e-15
  
    eMobilityAveraging=ElementEdge       
  hMobilityAveraging=ElementEdge       
  ParallelToInterfaceInBoundaryLayer(-ExternalBoundary)
  
    ##Method=Blocked
  ##SubMethod=ILS (set=1)
  Method=ILS (set=1)
  
  
  ILSrc = "
           set(1) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(0.0001,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };
     "

  number_of_threads=4


}


Solve {

   Poisson
  plugin { Poisson Electron Hole }
  Coupled { Poisson Electron Hole }

 NewCurrent="REV_"    
              	
   Quasistationary (  		DoZero
   			MaxStep=0.02  MinStep=1e-8 InitialStep=1e-5
			Increment=1.6 Decrement=4.0
                  		Goal { Name="anode" Voltage=-5000.0 } 
                  	BreakCriteria {Current (Contact = "anode" minval = -1e-6 )}
                 		)
                  { Coupled {  Poisson Electron Hole } } 
      

system("")

}
   

Plot {
	
        Current/Vector	
	eCurrent/Vector
	hCurrent/Vector
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	CurrentPotential
  	DopingConcentration	
	eMobility
	hMobility
	DonorConcentration
	AcceptorConcentration
 	AvalancheGeneration
 	
 	eAvalanche hAvalanche
 	eLifeTime hLifeTime


}


