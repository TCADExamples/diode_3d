
#setdep @node|IRRADIATION@


load_file n@node|IRRADIATION@_des.tdr   -name TDR

set dim 3
set position "0.0 0.0 200.0"
set vector "0 0 1"

create_plot -dataset TDR
select_plots Plot_TDR


set ff [calculate_field_value -plot Plot_TDR -geom TDR -field Abs(ElectricField-V) -max]
puts "DOE: Emax_at_XYZ $ff"


create_field -dataset TDR -name n -function max(<DopingConcentration>/(abs(<DopingConcentration>)+1.0),0)
create_field -dataset TDR -name p -function max((-1.0*<DopingConcentration>)/(abs(<DopingConcentration>)+1.0),0)

create_field -dataset TDR -name DEP -function <n>*<eDensity>/(<DopingConcentration>+10)-<p>*<hDensity>/(<DopingConcentration>-10)-0.05

set_field_prop -plot Plot_TDR -geom TDR DEP -show_bands

if {$dim==3} {
if {[lindex $vector 0]==1} {
create_cutplane -plot Plot_TDR -type y -at [lindex $position 1]
create_plot -dataset C1(TDR)
select_plots Plot_C1(TDR)
create_cutline -plot Plot_C1(TDR) -type z -at [lindex $position 2]
create_plot -dataset C1(C1(TDR)) -1d
select_plots Plot_C1(C1(TDR))
create_curve -plot Plot_C1(C1(TDR)) -dataset C1(C1(TDR)) -axisX X -axisY DEP
}
if {[lindex $vector 1]==1} {
create_cutplane -plot Plot_TDR -type x -at [lindex $position 0]
create_plot -dataset C1(TDR)
select_plots Plot_C1(TDR)
create_cutline -plot Plot_C1(TDR) -type z -at [lindex $position 2]
create_plot -dataset C1(C1(TDR)) -1d
select_plots Plot_C1(C1(TDR))
create_curve -plot Plot_C1(C1(TDR)) -dataset C1(C1(TDR)) -axisX Y -axisY DEP
}
if {[lindex $vector 2]==1} {
create_cutplane -plot Plot_TDR -type x -at [lindex $position 0]
create_plot -dataset C1(TDR)
select_plots Plot_C1(TDR)
create_cutline -plot Plot_C1(TDR) -type y -at [lindex $position 1]
create_plot -dataset C1(C1(TDR)) -1d
select_plots Plot_C1(C1(TDR))
create_curve -plot Plot_C1(C1(TDR)) -dataset C1(C1(TDR)) -axisX Z -axisY DEP
}

select_plots Plot_C1(C1(TDR))

set dist [get_curve_data Curve_1 -axisX  -plot Plot_C1(C1(TDR))]
set spacechargelimit [get_curve_data Curve_1 -axisY  -plot Plot_C1(C1(TDR))]

}

if {$dim==2} {
if {[lindex $vector 0]==1} {
create_cutline -plot Plot_TDR -type y -at [lindex $position 1]
create_plot -dataset C1(TDR) -1d
select_plots Plot_C1(TDR)
create_curve -plot Plot_C1(TDR) -dataset C1(TDR) -axisX X -axisY DEP
}
if {[lindex $vector 1]==1} {
create_cutline -plot Plot_TDR -type x -at [lindex $position 0]
create_plot -dataset C1(TDR) -1d
select_plots Plot_C1(TDR)
create_curve -plot Plot_C1(TDR) -dataset C1(TDR) -axisX Y -axisY DEP
}

select_plots Plot_C1(TDR)

set dist [get_curve_data Curve_1 -axisX  -plot Plot_C1(TDR)]
set spacechargelimit [get_curve_data Curve_1 -axisY  -plot Plot_C1(TDR)]

}


set listX ""
if {([lindex $spacechargelimit 0]< 0.0)} { set listX "$listX [lindex $dist 0]"; set flag 0 } else { set flag 1 }

for {set i 0} {$i<[llength $dist]} {incr i} {
	if { ([lindex $spacechargelimit $i]< 0.0)&&($flag==1) } {
		set listX "$listX [lindex $dist $i]"
		set flag 0
		}
	if { ([lindex $spacechargelimit $i] >= 0.0)&&($flag==0) } {
		set listX "$listX [lindex $dist $i]"
		set flag 1
		}
}

set listW ""
for {set i 0} {$i<[expr [llength $listX] -1]} {incr i} {
	set listW "$listW [expr abs([lindex $listX [expr $i +1]] - [lindex $listX $i])]"
}

puts "listW= $listW"

for {set i 0} {$i<[llength $listW]} {incr i} {
	puts "DOE: W${i} [format %.2f [lindex $listW $i]]"
}



