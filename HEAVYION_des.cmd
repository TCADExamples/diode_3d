#setdep @node|DIODE3DION@



File {
    Grid       = "n@node|DIODE3DION@_msh.tdr"
    Current    = "@plot@"
    Plot       = "@tdrdat@"
    output = "@log@"
    parameter = "@parameter@"
}



Electrode {
	!(
	for {set i 0} {$i<@NY@} {incr i} {
			for {set j 0} {$j<@NX@} {incr j} {
					puts "\{name = \"cathode${j}${i}\"   voltage = 0.0   eRecVelocity=1e7 hRecVelocity=1e7 \}"	
			}
	}
	)!
	
	###{name = "anode"   voltage = 0.0    voltage=((-1000 0)(-100 @<-HV>@) (0 @<-HV>@))	eRecVelocity=1e7 hRecVelocity=1e7 }					
	{name = "anode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }					
	
		
}


Physics { 
	##AreaFactor=1e8
	##Fermi
 }
        
 Physics (material="Silicon") { 

 	Mobility( 
		DopingDep(Unibo)
            	HighFieldsat(GradQuasiFermi)
 	  	)
  	Recombination( 
		 SRH(DopingDep TempDep)
		 Auger
  		##hAvalanche(UniBo) eAvalanche(UniBo)  		
  		hAvalanche eAvalanche 		
		)
  	EffectiveIntrinsicDensity(OldSlotboom)
 
  	 	Traps(
  			(
  			name="state1" acceptor conc=@<fluence*1.613>@
  			Level FromConductionBand	EnergyMid=0.42
  			eXsection=2E-15  hXsection=2E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state2" acceptor conc=@<fluence*100.0>@
  			Level FromConductionBand	EnergyMid=0.46
  			eXsection=5E-15  hXsection=5E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state3" donor conc=@<fluence*0.9>@
  			Level FromValenceBand	EnergyMid=0.36
  			eXsection=2.5E-14  hXsection=2.5E-15
			##eJfactor=1.0 hJfactor=1.0
			)
		)




  	HeavyIon("ion1") (
  		Direction=(@dirX@,@dirY@,@dirZ@)
  		!(
  		puts "Location=(@posX@,@posY@,@posZ@)"
  		)!
  		Time=5e-9
  		Length = @L@
  		LET_f = 0.01
  		Wt_hi = @whi@
  		Gaussian
  		PicoCoulomb
  		)

} 




CurrentPlot {
eLifeTime(Maximum(material="Silicon"))
hLifeTime(Maximum(material="Silicon"))
eAvalanche(Maximum(material="Silicon"))
hAvalanche(Maximum(material="Silicon"))
RadiationGeneration(Maximum(material="Silicon"))
HeavyIonGeneration(Maximum(material="Silicon"))

}

Math {


    Derivatives
  Avalderivatives
  Digits=7
  Notdamped=1000
  Iterations=15
  RelerrControl
  ErrRef(electron)=1e6
  ErrRef(hole)=1e6
  RhsMin=1e-15
  
  RecBoxIntegr
  
    eMobilityAveraging=ElementEdge       
  hMobilityAveraging=ElementEdge       
  ParallelToInterfaceInBoundaryLayer(-ExternalBoundary)

  Transient=BE
  
  
    Method=Blocked
  SubMethod=ILS (set=1)
  ACMethod=Blocked 
  ACSubMethod=ILS (set=2)
 
  
  ILSrc = "
           set(1) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(0.0001,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };
           
           set(2) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(1e-8,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };

           
     "

  number_of_threads=4

  Extrapolate
}


Solve {

#if 1==0
  Transient (  	initialtime=-1000 finaltime=0
   			MaxStep=100  MinStep=1e-15 InitialStep=1.0
			Increment=1.6 Decrement=4.0
			TurningPoints(		
			(  condition ( Time( -1e-9) )    value=1e-10 )
			(  condition ( Time( range=( -1e-9 0 )))   value=1e-10 )		

			)
                 )
                  { Coupled {  Poisson Electron Hole } } 
#endif
Poisson
  plugin { Poisson Electron Hole }
  Coupled { Poisson Electron Hole }

   Quasistationary (  		DoZero
   			MaxStep=0.1  MinStep=1e-8 InitialStep=1e-4
			Increment=1.6 Decrement=4.0
                  		Goal { Name="anode" Voltage=@<-HV>@ } 
                  	##BreakCriteria {Current (Contact = "anode" minval = -1e-9 )}
                 		)
                  { Coupled {  Poisson Electron Hole } } 
                  
 NewCurrent="HEAVY_"    
              	
   Transient (  	
   			##initialtime=-1000 finaltime=100.0e-9
   			initialtime=0 finaltime=100.0e-9

    			###MaxStep=1e-8
  			MaxStep=10 
   			MinStep=1e-18 InitialStep=1e-10
			Increment=1.6 Decrement=4.0
			TurningPoints(
			(  condition ( Time( range=( 0 @<5e-9 - 5e-11>@ )))   value=1e-10 )		
			(  condition ( Time( @<5e-9 - 5e-11>@) )    value=2e-12 )
			(  condition ( Time( range=( @<5e-9 - 5e-11>@ @<5e-9 + 5e-11>@ )))   value=2e-12 )		
			)
                 )
                  { 
                  	 Coupled {  Poisson Electron Hole } 
                  Plot(FilePrefix="n@node@_" time=(5e-9;5.02e-9;5.1e-9;5.2e-9;5.5e-9) nooverwrite )
                  } 


}
    

Plot {
	
        Current/Vector	
	eCurrent/Vector
	hCurrent/Vector
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	CurrentPotential
  	DopingConcentration	
	eMobility
	hMobility
	DonorConcentration
	AcceptorConcentration
 	AvalancheGeneration
 	
 	eAvalanche hAvalanche
 	eLifeTime hLifeTime
 	
 	RadiationGeneration
 	HeavyIonGeneration


}



