Title "Untitled"

Controls {
}

Definitions {
	AnalyticalProfile "Nplus" {
		Species = "PhosphorusActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+19, ValueAtDepth = 1e+12, Depth = 5)
		LateralFunction = Gauss(Factor = 0.1)
	}
	Constant "ConstantProfileDefinition_1" {
		Species = "BoronActiveConcentration"
		Value = 1e+12
	}
	Refinement "RefinementDefinition_1" {
		MaxElementSize = ( 132 10 1 )
		MinElementSize = ( 0.01 0.01 0.01 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
}

Placements {
	AnalyticalProfile "Place_Nplus_1" {
		Reference = "Nplus"
		ReferenceElement {
			Element = Line [(-40 0) (-10 0)]
		}
	}
	AnalyticalProfile "Place_Nplus_2" {
		Reference = "Nplus"
		ReferenceElement {
			Element = Line [(10 0) (40 0)]
		}
	}
	Constant "ConstantProfilePlacement_1" {
		Reference = "ConstantProfileDefinition_1"
		EvaluateWindow {
			Element = material ["Silicon"]
		}
	}
	Refinement "RefinementPlacement_1" {
		Reference = "RefinementDefinition_1"
		RefineWindow = material ["Silicon"]
	}
}

