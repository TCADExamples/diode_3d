
if {![info exists symbol]} {
global symbol
global color
global colorindex
global symbolindex
set symbol "square circle diamond plus cross splus scross triangle"
set color "black red blue green brown violet pink"

set Imin 1e10
set Imax -1e10
set colorindex 0
set symbolindex 0

proc nextcolor { } {
global symbol
global color
global colorindex
global symbolindex
	if {$colorindex<[llength $color]} {
		set linecolor [lindex $color $colorindex]
		incr colorindex
		} else {
		set colorindex 0
		set linecolor [lindex $color $colorindex]		
		}
	return $linecolor
	}
proc nextsymbol { } {
global symbol
global color
global colorindex
global symbolindex
	if {$symbolindex<[llength $symbol]} {
		set symboltype [lindex $symbol $symbolindex]
		incr symbolindex
		} else {
		set symbolindex 0
		set symboltype [lindex $symbol $symbolindex]		
		}
	return $symboltype
	}
}




#setdep @node|REVERSE@

set N @node@

set fileBV1 "REV_n@node|REVERSE@_des.plt"

set col [nextcolor]
set symb [nextsymbol]


proj_load $fileBV1  PLT($N)_BV1

set curvename "BVak1_PLT($N)_@fluence@"
cv_createDS $curvename "PLT($N)_BV1 anode InnerVoltage" "PLT($N)_BV1 anode TotalCurrent" y

cv_setCurveAttr $curvename $curvename $col solid 2 $symb 6  $col 1  $col

if {![info exists cv_index]} {
set curvename3 "BVakext_PLT($N)_@fluence@"
cv_createDS $curvename3 "PLT($N)_BV1 anode TotalCurrent" "PLT($N)_BV1 anode InnerVoltage" y

set BV1 [expr abs([cv_compute "vecmax(<$curvename3>)" A A A A ])]
set BV2 [expr abs([cv_compute "vecmin(<$curvename3>)" A A A A ])]
if {$BV1<$BV2} {set BV $BV2 } else {set BV $BV1}

puts "BV = $BV"

ft_scalar BVak [format %.1f $BV]
}




set Imint [cv_compute "vecmin(<$curvename>)" A A A A ]
##set Imaxt [cv_compute "vecmax(<$curvename2>)" A A A A ]
if {$Imint<$Imin} {set Imin $Imint }
##if {$Imaxt>$Imax} {set Imax $Imaxt }



gr_setTitleAttr "BVce"

gr_setAxisAttr X  {Vce  (V)}     16  {} {} black 1 14 0 5 0
##gr_setAxisAttr Y  {Ice (A)} 16  [expr 1.1*$Imin] [expr 1.1*$Imax] black 1 14 0 5 0
gr_setAxisAttr Y  {Ice (A)} 16  [expr 1.1*$Imin] {} black 1 14 0 5 0


