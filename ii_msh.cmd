Title "Untitled"

Controls {
}

Definitions {
	AnalyticalProfile "Nplus" {
		Species = "PhosphorusActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+19, ValueAtDepth = 1e+12, Depth = 5)
		LateralFunction = Gauss(Factor = 0.1)
	}
}

Placements {
	AnalyticalProfile "Place_Nplus_1" {
		Reference = "Nplus"
		ReferenceElement {
			Element = Line [(-40 0) (-10 0)]
		}
	}
	AnalyticalProfile "Place_Nplus_2" {
		Reference = "Nplus"
		ReferenceElement {
			Element = Line [(10 0) (40 0)]
		}
	}
}

