
(sde:clear)

(define W 200)

(define Lstripe_pitchX 50.0)
(define LNplus_stripeX 30.0)

(define Lstripe_pitchY 50.0)
(define LNplus_stripeY 30.0)


(define NX @NX@)
(define NY @NY@)


(define LX (* NX Lstripe_pitchX))
(define LY (* NY Lstripe_pitchY))

(define Plow 1e12)
(define Nca 1e19)
(define Pan 1e19)
(define Xj 4.0)


(define mpoly (list))

(
do ((i 0 (+ i 1))) ((>= i NY))
	(
	do ((j 0 (+ j 1))) ((>= j NX))
		(define ff (list 
				(+ (* j Lstripe_pitchX) (* 0.5 (- Lstripe_pitchX LNplus_stripeX))) (+ (* i Lstripe_pitchY) (* 0.5 (- Lstripe_pitchY LNplus_stripeY)))
				(+ (* j Lstripe_pitchX) (* 0.5 (+ Lstripe_pitchX LNplus_stripeX))) (+ (* i Lstripe_pitchY) (* 0.5 (- Lstripe_pitchY LNplus_stripeY)))
				(+ (* j Lstripe_pitchX) (* 0.5 (+ Lstripe_pitchX LNplus_stripeX))) (+ (* i Lstripe_pitchY) (* 0.5 (+ Lstripe_pitchY LNplus_stripeY)))
				(+ (* j Lstripe_pitchX) (* 0.5 (- Lstripe_pitchX LNplus_stripeX))) (+ (* i Lstripe_pitchY) (* 0.5 (+ Lstripe_pitchY LNplus_stripeY)))
		))
	(set! mpoly (append mpoly (list ff)))
	)
)

(sdepe:generate-mask "DD" mpoly )


(sdegeo:create-cuboid (position 0 0 0) (position LX LY W) "Silicon" "RSub")


(sdepe:pattern "mask" "DD" "polarity" "dark" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep" )


(sdedr:define-gaussian-profile "Nplus" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" Xj "Gauss"  "Factor" 0.8)

(sdepe:implant "Nplus" "flat")

(entity:delete (find-material-id "Resist"))


(sdepe:pattern "mask" "DD" "polarity" "light" "material" "Aluminum" "thickness" 0.5  "type" "aniso" "algorithm" "sweep" )


(define gg (entity:copy (find-mask "DD")))
(sde:attrib-remove gg "maskname")
(generic:add gg "maskname" "DD2")
(sde:offset-mask "DD2" 5.0)


(sdepe:fill-device "material" "Oxide")

(sdepe:pattern "mask" "DD2" "polarity" "light" "material" "Aluminum" "thickness" 0.5  "type" "aniso" "algorithm" "sweep" )

(sdepe:fill-device "material" "Oxide")

(bool:unite (find-material-id "Oxide"))
(bool:unite (find-material-id "Aluminum"))
(sde:separate-lumps)

(
do ((i 0 (+ i 1))) ((>= i NY))
	(
	do ((j 0 (+ j 1))) ((>= j NX))
		(sde:add-material (find-body-id 
			(position (+ (* j Lstripe_pitchX) (* 0.5 Lstripe_pitchX)) (+ (* i Lstripe_pitchY) (* 0.5 Lstripe_pitchY)) (+ W 0.1)))
		"Aluminum" (string-append "RAl" (number->string j) (number->string i)))
		
		(sdegeo:define-contact-set (string-append "cathode" (number->string j) (number->string i)) 4  (color:rgb 1 0 0 ) "##")

		(sdegeo:set-current-contact-set (string-append "cathode" (number->string j) (number->string i)) )
		(sdegeo:set-contact-boundary-faces (find-region-id (string-append "RAl" (number->string j) (number->string i))))

	)
)




(sdedr:define-refeval-window "RefEval_anode" "Rectangle"  (position 0 0 0) (position LX LY 0))

(sdedr:define-gaussian-profile "DopAnode" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" 5 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "Place_anode" "DopAnode" "RefEval_anode" "Both" "NoReplace" "Eval")



(sdegeo:define-contact-set "anode" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:set-current-contact-set "anode")
(sdegeo:set-contact-faces (find-face-id (position 0.1 0.1 0)))


(entity:delete (find-material-id "Aluminum"))






(sdedr:define-constant-profile "ConstantProfileDefinition_1" "PhosphorusActiveConcentration" Plow)
(sdedr:define-constant-profile-material "ConstantProfilePlacement_1" "ConstantProfileDefinition_1" "Silicon")


(sdedr:define-refinement-size "RefinementDefinition_1" (* 0.1 LX) (* 0.1 LY) (/ W 20.0) 0.5 0.5 0.5 )
(sdedr:define-refinement-placement "RefinementPlacement_1" "RefinementDefinition_1" (list "material" "Silicon" ) )
(sdedr:define-refinement-function "RefinementDefinition_1" "DopingConcentration" "MaxTransDiff" 1)

(sdegeo:translate-selected (get-body-list) (transform:translation (gvector (- 0 (* 0.5 LX)) (- 0 (* 0.5 LY)) 0)) #f 0)
(sdegeo:translate-selected (get-drs-list) (transform:translation (gvector (- 0 (* 0.5 LX)) (- 0 (* 0.5 LY)) 0)) #f 0)

(sde:build-mesh "snmesh" " " "n@node@_msh")







